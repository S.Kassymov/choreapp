package com.example.choreapp.module

val DATABASE_VERSION: Int = 1
val DATABASE_NAME:String ="chore.db"
val TABLE_NAME:String="chores"

val KEY_ID:String="id"
val KEY_CHORE_NAME:String="chore_name"
val KEY_CHORE_ASSIGED_BY:String="assigned_by"
val KEY_CHORE_ASSIGED_TO:String="assigned_to"
val KEY_CHORE_ASSIGNED_TIME:String="assigned_time"