package com.example.choreapp.data

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import com.example.choreapp.module.*
import java.text.DateFormat
import java.util.*

class ChoresDatabaseHandler(context: Context):
        SQLiteOpenHelper(context, DATABASE_NAME,null, DATABASE_VERSION) {

    override fun onCreate(db: SQLiteDatabase?) {
        var CREATE_CHORE_TABLE ="CREATE TABLE "+ TABLE_NAME+"("+ KEY_ID+" INTEGER PRIMARY KEY, "+
                KEY_CHORE_NAME+" TEXT,"+
                KEY_CHORE_ASSIGED_BY+" TEXT,"+
                KEY_CHORE_ASSIGED_TO+" TEXT,"+
                KEY_CHORE_ASSIGNED_TIME+" LONG"+")"
        db?.execSQL(CREATE_CHORE_TABLE)
    }

    fun createChore(chore:Chore){
        var db : SQLiteDatabase= writableDatabase

        var values: ContentValues=ContentValues()
        values.put(KEY_CHORE_NAME, chore.choreName)
        values.put(KEY_CHORE_ASSIGED_BY,chore.assignedBy)
        values.put(KEY_CHORE_ASSIGED_TO,chore.assignedTo)
        values.put(KEY_CHORE_ASSIGNED_TIME,System.currentTimeMillis())

        db.insert(TABLE_NAME, null, values)

        Log.d("DATA INSERTED","SUCCESS")
        db.close()
    }

    fun readChore(id: Int):Chore{
        var db: SQLiteDatabase= writableDatabase
        var cursor: Cursor =db.query(TABLE_NAME, arrayOf(KEY_ID, KEY_CHORE_NAME,
                                     KEY_CHORE_ASSIGED_BY, KEY_CHORE_ASSIGED_TO,
                                     KEY_CHORE_ASSIGNED_TIME), KEY_ID + "=?", arrayOf(id.toString()),
                                     null,null,null,null)

        if(cursor!=null)
            cursor.moveToFirst()

        var chore=Chore()
        chore.choreName=cursor.getString(cursor.getColumnIndex(KEY_CHORE_NAME))
        chore.assignedBy=cursor.getString(cursor.getColumnIndex(KEY_CHORE_ASSIGED_BY))
        chore.assignedTo=cursor.getString(cursor.getColumnIndex(KEY_CHORE_ASSIGED_TO))
        chore.timeAssigned=cursor.getLong(cursor.getColumnIndex(KEY_CHORE_ASSIGNED_TIME))

        var dateFormat:java.text.DateFormat= DateFormat.getDateInstance()
        var formattedDate= dateFormat.format(Date(cursor.getLong(cursor.getColumnIndex(KEY_CHORE_ASSIGNED_TIME))).time)

        return chore
    }


    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }

}